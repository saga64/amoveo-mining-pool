-module(workers).
-behaviour(gen_server).
-export([start_link/0,code_change/3,handle_call/3,handle_cast/2,
		 handle_info/2,init/1,terminate/2]).

-export([update_worker_share/1, miner_overview/0, miner_detail/1]).

-record(worker, {total_shares = 0, start_time = 0, last_time = 0}).

init(ok) ->  {ok, dict:new()}.

start_link() -> gen_server:start_link({local, ?MODULE}, ?MODULE, ok, []).
code_change(_OldVsn, State, _Extra) -> {ok, State}.
terminate(_, _) -> io:format(" Workers Module Died!"), ok.
handle_info(_, X) -> {noreply, X}.

update_worker_share({Pubkey, WorkerID}) ->
	gen_server:cast(?MODULE, {update_worker_share, {Pubkey, WorkerID}}).
miner_overview() -> gen_server:call(?MODULE, miner_overview).
miner_detail(Pubkey) -> gen_server:call(?MODULE, {miner_detail, Pubkey}).

handle_cast({update_worker_share, {Pubkey, WorkerID}}, X) ->
	%io:fwrite("update_worker_share "),
	%io:fwrite("Pubkey is  "),
	%io:fwrite(packer:pack(Pubkey)),
	%io:fwrite("  WorkerID is  "),
	%io:fwrite(WorkerID),
	%io:fwrite(" \n "),

	{M, Ss, _} = os:timestamp(),
	Time = M * 1000000 + Ss,
	Key = {Pubkey, WorkerID},
	N = case dict:find(Key, X) of
			error -> #worker{total_shares = 1, start_time = Time, last_time = Time};
			{ok, W} ->
				if
					Time - W#worker.last_time > 36000 ->
						%clean it up
						#worker{total_shares = 1, start_time = Time, last_time = Time};
					Time - W#worker.start_time > 36000 ->
						TotalShares = W#worker.total_shares + 1,
						StartTime = W#worker.start_time + (Time - W#worker.last_time) / 3,
						TotalShares1 = TotalShares * 2 / 3,
						#worker{total_shares = TotalShares1, start_time = StartTime, last_time = Time};
					true ->
						TotalShares = W#worker.total_shares + 1,
						#worker{total_shares = TotalShares, start_time = W#worker.start_time, last_time = Time}
				end
		end,
	X2  = dict:store(Key, N, X),
	{noreply, X2};

handle_cast(_, X) -> {noreply, X}.


handle_call(miner_overview, _From, X) ->
	Online_Workers_Num = dict:size(X),
	{TotalHR, TotalRecentShares, Active_Accounts} = dict:fold(fun(K, V, Acc) ->
						{Pubkey, _WorkerID} = K,
						{H, S, D} = Acc,
						D1 = dict:store(Pubkey, 1, D),
						HR = if 
							V#worker.last_time =< V#worker.start_time -> 
									0;
								true ->
									V#worker.total_shares / (V#worker.last_time - V#worker.start_time)
							end,
						H1 = H + HR,
						S1 = S + V#worker.total_shares,
					   {H1, S1, D1}
				   end, {0, 0, dict:new()}, X),
	Active_Accounts_Num = dict:size(Active_Accounts),
	{reply, [TotalHR, TotalRecentShares, Active_Accounts_Num, Online_Workers_Num], X};

handle_call({miner_detail, Pubkey}, _From, X) ->
	{MS, Ss, _} = os:timestamp(),
	Time = MS * 1000000 + Ss,
	% filter out the worker belongs to this pubkey
	X2 = dict:filter(fun(K, V) ->
							 {K1, _K2} = K,
							 (K1 == Pubkey) and (Time - V#worker.last_time < 36000)
					 end, X),

	Online_Workers_Num = dict:size(X2),
	% calculate each worker's hashrates
	{Total_HR, TotalRecentShares, Online_Workers_List} = dict:fold(fun(K, V, Acc) ->
						   {H, S, L} = Acc,
						   {_, WorkerID} = K,
						   io:fwrite(WorkerID),
						   HR = if 
									V#worker.last_time =< V#worker.start_time -> 
										0;
									true ->
										V#worker.total_shares / (V#worker.last_time - V#worker.start_time)
								end,
						   H1 = H + HR,
						   S1 = S + V#worker.total_shares,
						   {H1, S1, lists:append(L, [{WorkerID, HR, V#worker.total_shares, V#worker.last_time}])}
				   end, {0, 0, []} , X2),


	{reply, [Total_HR, TotalRecentShares, Online_Workers_Num, Online_Workers_List], X};


handle_call(_, _From, X) -> {reply, X, X}.
