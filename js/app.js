var instance = M.Tabs.init(document.getElementById('tabs'), {});

function showServerInfo() {
    var server_ip = document.URL.split("/")[2];
    var port = 80;
    document.getElementById('server-ip').value = server_ip;
    document.getElementById('server-port').value = port;
}

function showTotalShare() {
    variable_public_get(["account", 2], function(total) {
        document.getElementById('total-shares').innerHTML = total;
    });
}

function showMinerOverview() {
    variable_public_get(["miner_overview"], function(x) {
        var total_hashrate = x[1];
        var total_recentshare = x[2];
        var total_accounts = x[3];
        var total_workers = x[4];

        document.getElementById('miner-overview').innerHTML = "<table><tr><th>矿池算力</th><th>最近Shares</th><th>在线账户</th><th>矿机数量</th></tr>" +
            "<tr><td>" + (3500.0 * total_hashrate).toFixed(2) + "GH/s</td><td>" +total_recentshare+ "</td><td>" + total_accounts + "</td><td>" + total_workers + "</td></tr></table>"
    });
}

function lookupAccount() {
    var walletAddress= document.getElementById('wallet-address').value;
    setCookie('wallet_address', walletAddress, 30);

    console.log("lookup account");
    variable_public_get(["account", walletAddress], account_info);

    function account_info(x) {
        var veo = x[2];
        var shares = x[3];
        document.getElementById('account-info').innerHTML = "<h3>账户信息</h3>" +
            "<table><tr><th>Veo</th><th>Shares</th></tr><tr><td>" + (parseFloat(veo) / 100000000.0).toFixed(5) + "</td><td>" + shares + "</td></tr></table>";
    }
}

function showWorkersList() {
    var walletAddress= document.getElementById('wallet-address').value;
    variable_public_get(["miner_detail", walletAddress], miner_details);

    function miner_details(x) {
        var total_hashrate = x[1];
        var total_recentshare = x[2];
        var online_workers = x[3];
        var minerDetails = document.getElementById('miner-details');

        minerDetails.innerHTML =
            "</br>在线矿机数量: ".concat(online_workers).
            concat(" 矿机总算力: ").concat((3500.0*total_hashrate).toFixed(2)).
            concat(" GH/s");

        var table_HTML = "".
        concat("<table>").
        concat("<tr><th>Worker</th><th>Hashrates</th><th>Recent Shares</th><th>Last Submit</th></tr>");

        var online_workers_list = x[4];
        online_workers_list.shift(); //remove first element


        for (var i in online_workers_list) {
            var worker_info = online_workers_list[i];
            var worker_name = worker_info[1];
            worker_name.shift();
            for (y in worker_name) {
                worker_name[y] = String.fromCharCode(worker_name[y]);
            }
            worker_name = worker_name.join("");
            worker_info[1] = worker_name;
            online_workers_list[i] = worker_info;
        }

        online_workers_list.sort(function(a,b){
            if(a[1] < b[1]) return -1;
            if(a[1] > b[1]) return 1;
            return 0;
        });

        for (i in online_workers_list) {
            var worker_info = online_workers_list[i];
            var worker_name = worker_info[1];

            var raw_hr = worker_info[2];
            var hashrates = (3500.0*raw_hr).toFixed(2);
            var worker_shares = worker_info[3];
            var worker_updatetime = worker_info[4];
            var newDate = new Date();
            newDate.setTime(worker_updatetime * 1000);
            var worker_updatetime_str = newDate.toString();
            table_HTML = table_HTML.
            concat("<tr>").
            concat("<td>").concat(worker_name).concat("</td>").
            concat("<td>").concat(hashrates).concat(" Gh/s</td>").
            concat("<td>").concat(worker_shares.toFixed(2)).concat("</td>").
            concat("<td>").concat(worker_updatetime_str).concat("</td>").
            concat("</tr>");
        }
        table_HTML = table_HTML.concat("</table>");
        minerDetails.innerHTML += table_HTML;
    }
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function loadAccountFromCookie() {
    var walletAddress = getCookie('wallet_address');

    if (walletAddress) {
        document.getElementById('wallet-address').value = walletAddress;
        lookupAccount();
        showWorkersList();
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    showServerInfo();
    showMinerOverview();
    showTotalShare();
    loadAccountFromCookie();

    document.getElementById("lookup-account-btn").addEventListener("click", function(event){
        event.preventDefault();
        lookupAccount();
        showWorkersList();
    });
});