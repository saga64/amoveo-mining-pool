#!/bin/bash
echo "Creating session"
/usr/bin/tmux new-session -d -s payment
echo "Attaching to pool"
/usr/bin/tmux send-keys -t payment "cd /home/ubuntu/pool && sh attach.sh" C-m
sleep 3

echo "Convert share to veo"
/usr/bin/tmux send-keys -t payment "accounts:got_reward()." C-m

sleep 10

echo "Sending payment"
/usr/bin/tmux send-keys -t payment "accounts:pay_veo()." C-m

sleep 10

echo "Save accounts"
/usr/bin/tmux send-keys -t payment "accounts:save()." C-m

sleep 5 

echo "Dettach from pool"
/usr/bin/tmux send-keys -t payment "" C-d
sleep 1

echo "Ending session"
/usr/bin/tmux kill-session -t payment
