#!/usr/bin/env python
#encoding=utf-8
# Author: Aaron Shao - shao.dut#gmail
# Last modified: 2018-05-22 23:11
# Filename: checker.py
# Description: 
import json
import requests
import sys
import time

def query_node(data, full_node_api = "http://127.0.0.1:8080/"):
#    full_node_api = "http://114.215.136.52:8080/"
    json_data=json.dumps(data)
    r=requests.post(full_node_api,data=json_data)
    return json.loads(r.text)


def get_current_block_height():
    data = ["height"]
    r = query_node(data)
    if r[0] != "ok":
        print >> sys.stderr, "get_current_block_height error! ", json.dumps(r)
        return 0
    return r[1]

def get_remote_block_height():
    data = ["height"]
    r = query_node(data, "http://45.77.196.69:8080")
    if r[0] != "ok":
        print >> sys.stderr, "get_remote_block_height error! ", json.dumps(r)
        return 0
    return r[1]

    
def get_block_by_height(N):
    data = ["block", N]
    r = query_node(data)
    if r[0] != "ok":
        print >> sys.stderr, "get_block_by_height error! ", json.dumps(r)
        return 0
    return r[1]

def get_pool_trx_from_block(block):
    raw_trxs = []
    try:
        raw_trxs = block[10][1:]
    except Exception,e:
        print >> sys.stderr, "get_pool_trx_from_block error, ", e
        print >> sys.stderr, block
    current_height =  get_current_block_height()
    raw_trxs = filter(lambda x: x[0]==u'signed' and 
            x[1][1]==u'BLu2W4Lw39b5aHmtCFRdqjmBvqiJ9fbpxQCg3sU61Xa2SfJWpI/alJCsbBHkzt++Ly3s9f8WNAEtI2tB0GoVicc=',
            raw_trxs)
    trxs = {}
    for t in raw_trxs:
        miner_addr = t[1][4]
        amount = t[1][5]
        if miner_addr not in trxs:
            trxs[miner_addr] = amount
        else:
            trxs[miner_addr] = trxs[miner_addr] + amount
    return trxs

def get_pool_trx_last_N_block(N):
    current_height =  get_current_block_height()
    if N > current_height:
        return {}
    trxs = {}
    blocks = get_blocks_many(N, current_height-N)
    print >> sys.stderr, "Getting Trxs from Block from ", current_height-N, "to ", 
    print >> sys.stderr, current_height, " Total :", len(blocks)
    for block in blocks:
        trxs_in_block = get_pool_trx_from_block(block)
        for t in trxs_in_block:
            if t not in trxs:
                trxs[t] = trxs_in_block[t]
            else:
                trxs[t] = trxs[t] + trxs_in_block[t]
    return trxs 


def get_pool_trx_from_N(N):
    step = 100
    current_height =  get_current_block_height()
    if N > current_height:
        return {}
    trxs = {}
    while N < current_height:
        blocks = get_blocks_many(step, N)
        print >> sys.stderr, "Getting Trxs from Block from ",N, "to ",  N+step
        for block in blocks:
            trxs_in_block = get_pool_trx_from_block(block)
            for t in trxs_in_block:
                if t not in trxs:
                    trxs[t] = trxs_in_block[t]
                else:
                    trxs[t] = trxs[t] + trxs_in_block[t]
        N = N+step
        time.sleep(3)
    return trxs 



def save_block_trxs(fname, trxs):
    with open(fname, "w") as f_out:
        for t in trxs:
            f_out.write("%s\t%d\n" % (t, trxs[t]))

def get_blocks_many(many, N):
    data = ["blocks", many, N]
    r = query_node(data)
    if r[0] != "ok":
        print >> sys.stderr, "get_blocks_many error! ", json.dumps(r)
        return 0
    return r[1][1:]


def load_block_trxs():
    trxs = {}
    with open("block_trxs.txt", "r") as f_in:
        for line in f_in:
            items = line.strip().split("\t")
            trxs[items[0]] = int(items[1])
    return trxs

def load_pool_trxs(fname="spend.log"):
    trxs = {}
    with open("spend.log", "r") as f_in:
        for line in f_in:
            items = line.strip().split(" ")
            addr = items[0]
            amount = int(items[1])
            if addr not in trxs:
                trxs[addr] = amount
            else:
                trxs[addr] = trxs[addr] + amount
    return trxs

def diff_pool_block_trxs(pool_trxs, block_trxs):
    diff_trx = {}
    for t in pool_trxs:
        if t in block_trxs:
            pool_trxs[t] = pool_trxs[t] - block_trxs[t]
    for t in pool_trxs:
        if pool_trxs[t] > 0:
            diff_trx[t] = pool_trxs[t]
    return diff_trx

if "__main__" == __name__:
    current_height =  get_current_block_height()
    #remote_height  =  get_remote_block_height()
    #print "current_height is ", current_height, " remote_height is ", remote_height
    #if current_height < remote_height:
    #    print >> sys.stderr, "Node note synced! ABORT !"
    #    sys.exit(-1)
    #trxs =  get_pool_trx_last_N_block(300)
    #trxs =  get_pool_trx_from_N(21675)
    trxs =  get_pool_trx_from_N(24200)
    save_block_trxs("block_trxs.txt", trxs)
    block_trxs = load_block_trxs()
    pool_trxs  = load_pool_trxs()
    diff = diff_pool_block_trxs(pool_trxs, block_trxs)
    save_block_trxs("diff_trxs.txt", diff)
    total_diff = 0
    for t in diff:
        total_diff += diff[t]
        print "api:spend(base64:decode(<<\"%s\">>), %d)." % (t, diff[t])

    print "Total ", total_diff 
    

